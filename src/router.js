import Vue from 'vue'
import Router from 'vue-router'
import Landing from './views/Landing.vue'

Vue.use(Router)

export default new Router({
	mode: 'history',
	base: process.env.BASE_URL,
	routes: [
		{
			path: '/',
			name: 'landing',
			component: Landing
		},
		{
			path: '/ce',
			name: 'call-employee',
			component: () => import('./views/CallEmployee.vue')
		},
		{
			path: '/cw',
			name: 'call-worker',
			component: () => import('./views/CallWorker.vue')
		}
	]
})